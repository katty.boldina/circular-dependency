# Project, demonstrating solving of the circular dependency.

## Circular dependency - 
The constructor of your class A calls the constructor of class B. 
The constructor of class B calls the constructor of class A. 
You have an infinite recursion call, that's why you end up having a StackOverflowError.
(BeanCurrentlyInCreationException)

- com.circular.dependent_classes.impl.SubjectServiceImpl_Dependent
- com.circular.dependent_classes.impl.StudentServiceImpl_Dependent

## Variant 1
spring.main.allow-circular-references=true
Or use the `@Lazy` annotation for lazy initialization. But these are only temporary solutions.

- com.circular.dependent_classes_lazy.impl.StudentServiceImpl_Lazy
- com.circular.dependent_classes_lazy.impl.SubjectServiceImpl_Lazy

## Variant 2
Configure circular dependencies with setter injection. 

- com.circular.dependent_classes_setter.impl.StudentServiceImpl_Setter
- com.circular.dependent_classes_setter.impl.SubjectServiceImpl_Setter

## Variant 3
Refactor.