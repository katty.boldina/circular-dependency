package com.circular.dependent_classes.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl_Dependent {

    private final SubjectServiceImpl_Dependent subjectService;
}
