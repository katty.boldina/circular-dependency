package com.circular.dependent_classes.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl_Dependent {

    private final StudentServiceImpl_Dependent studentService;
}
