package com.circular.dependent_classes_lazy.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Lazy
@Service
@RequiredArgsConstructor
public class StudentServiceImpl_Lazy {

    private final SubjectServiceImpl_Lazy subjectServiceImpl_lazy;

}
