package com.circular.dependent_classes_lazy.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Lazy
@Service
@RequiredArgsConstructor
public class SubjectServiceImpl_Lazy {

    private final StudentServiceImpl_Lazy studentServiceImpl_setter;
}
