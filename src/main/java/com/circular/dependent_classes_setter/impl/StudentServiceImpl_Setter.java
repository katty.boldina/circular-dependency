package com.circular.dependent_classes_setter.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl_Setter {

    private SubjectServiceImpl_Setter subjectServiceImpl_setter;

    /**
     * Setter dependency injection.
     *
     * @param subjectServiceImpl_setter {@link SubjectServiceImpl_Setter}
     */
    public void setMovieFinder(SubjectServiceImpl_Setter subjectServiceImpl_setter) {
        this.subjectServiceImpl_setter = subjectServiceImpl_setter;
    }

}
