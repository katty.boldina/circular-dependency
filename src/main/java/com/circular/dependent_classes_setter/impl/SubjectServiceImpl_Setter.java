package com.circular.dependent_classes_setter.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl_Setter {

    private final StudentServiceImpl_Setter studentServiceImpl_setter;
}
